package com.joyce.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * 验证刷新步骤：
 * 1、http://localhost:8065/getAbc  请求获取abc值
 * 2、更改git库里foo值
 * 3、http://localhost:8065/refresh 刷新某个微服务里的值，需类里加上注解@RefreshScope，并且jar依赖有spring-boot-starter-actuator
 * 4、http://localhost:8065/getAbc  获取新的abc值
 * @author zhuwen
 *
 */
@RestController
@RefreshScope
public class ClientController {

	@Value("${abc}")
	private String abc;
	
	@RequestMapping(value="/getAbc")
	public String getAbc(){
		return "abc===" + abc;
	}
}
