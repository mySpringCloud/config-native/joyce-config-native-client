package com.joyce;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

/**
 * 测试以http形式读取config server端
 * @author zhuwen
 *
 */
public class HttpClientTest {

	public static void main(String[] args) throws ClientProtocolException, IOException {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet("http://localhost:8060/joyce-config-svn-client-dev.properties");
		//执行请求
		CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
		System.out.println(httpResponse.getStatusLine().getStatusCode());
		//解析返回
		HttpEntity httpEntity = httpResponse.getEntity();
		InputStream inputStream = httpEntity.getContent();
		byte[] bytes = new byte[(int)httpEntity.getContentLength()];
		inputStream.read(bytes);
		System.out.println("返回："+new String(bytes));
	}

}
